# goals

goals is a simple django app designed to help its users achieve their life goals.

Users of the app can perform the following operation:
    
1) Create different lists to group their goals.

2) Create their life goals under each list.

3) Rename and Delete list and goals.

Degree of freedom available to users to perform this operations are limited by subscriptions which are outlined below.

|      Plans        |   Number of list(max) | Number of goals(max)  | Rename Flag    | Remove flag       |
|-------------------|----------------|-----------------------|----------------|----------------------|                    
|    Basic Plan     |        1           |         7             |    False       |  False          |
|    Starter Plan   |        1           |         12            |    False       |  False          |
|    Premium Plan   |       inf          |         inf           |    True        |  True           |

----------------------------------------------------------------------------------------------------------------

## Requirements

Python 3

django 1.11

database = mysql MariaDb

mysqlclient

	 Getting Python

1)    Python Installer can be found on  https://www.python.org/downloads/ .

2)    Download the appropriate installer for your OS and install.

3)    To verify successful installation, search for "python" from your explorer and click on it to see a python shell.
	
	 Getting django 1.11

1)    To install django, python 3 must have been successfully installed.

2)    Open a command line interface and type:

> pip install django==1.11		
	
3)    To verify successful installation, open the python shell again and type:

> \>>> import django 						

> \>>> django.get_version()
	
4)    Step 3 should successfully return '1.11', if not, kindly repeat from step 1.

	 Getting mysql MariaDb

1)    Download and Install Xampp

     Getting mysqlclient

1)    To install mysqlclient, python 3 must have been successfully installed and Microsoft Visual C++ extensions updated up to C++ 14 on windows.

2)    Open a command line interface and type:

> pip install mysqlclient


---------------------------------------------------------------------------------------------------------------------------------




## Quickstart

Please note that this was written for windows

### Importing the database

1) Create a folder named **my_life_goals** anywhere on your system, preferably location that does not require elevated privileges.

2) Clone the gitlab repository into the folder **my_life_goals**.

3) The Database export is named **goals.sql**. The sql export should be in the root folder **~/my_life_goals/**, move it to the **bin** folder of mysql (**~\Xampp\mysql\bin**) for importing.

4) To start the mysql server, open a command line, cd to **~\Xampp\mysql\bin**, and issue this command:
> mysqld

5) If the previous steps were done properly, you should  the mysql server process starting up on PID assigned by your OS and port 3306. You can close the command line 

6) Open another command line, and cd to **~\Xampp\mysql\bin**, and issue this command:
> mysql -u root -p

7) You would be prompted for a password, if a first time user of mysql, press the enter key to continue to the mysql shell, otherwise provide a root user password.

8) The mysql shell should look like: 
> MariaDB [(none)]>

9) Create the required database and user with this command:
> MariaDB [(none)]> create database GOALS default character set utf8 default collate utf8_bin;

> MariaDB [(none)]> grant all privileges on GOALS.* to ADMIN@localhost identified by 'django';

10) Exit the mysql shell with Ctrl+C and import the database with this mysql command:
    
> mysql -u ADMIN -p -h localhost Goals < goals.sql		 

**password: django**

### Running the web-app
	
1) Open another command line, cd to **my_life_goals** folder created earlier and run the following command:
    
> python manage.py runserver
	
2) Wait for some seconds for the django server to start running. Upon successful start, your should see no error message

3) Open a browser and go to: [127.0.0.1:8000/goals](127.0.0.1:8000/goals). If zero error occured, the browser should display the homepage of the django app.
