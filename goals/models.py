from django.db import models
from django.contrib.auth.models import User

class Profile(models.Model):  #Model for extra user information
    owner = models.ForeignKey(User,default = 1,on_delete = models.CASCADE)
    sub = models.CharField(max_length = 100,default = "Basic")   #sub plan
    n_list = models.IntegerField(default = 0)
    n_goals = models.IntegerField(default = 0)
    def __str__(self):
        return (self.sub)

class Goals_list(models.Model):  #Model to store users different list of goals
    owner = models.ForeignKey(User,default = 1,on_delete = models.CASCADE)
    title = models.CharField(max_length = 250)
    def __str__(self):
        return self.title
    
class Goals(models.Model):   #Model to store goals users
    category = models.ForeignKey(Goals_list,on_delete = models.CASCADE)
    text = models.CharField(max_length = 450)
    hit = models.IntegerField(default=0)
    def __str__(self):
        return self.text