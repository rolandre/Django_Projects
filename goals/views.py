'''
Three subscription has been created for the To-do List:
            Number_of_list(max) | Number_of_goals(max)  | Rename_Flag    | Remove_flag
Basic Plan          1           |         7             |    False       |  False
Starter Plan        1           |         12            |    False       |  False
Premium Plan       inf          |         inf           |    True        |  True
'''
#Models
from goals.models import Goals,Goals_list,Profile
from django.contrib.auth.models import User
#from django.contrib.auth.models import User
#Others
from django.shortcuts import get_object_or_404
from django.contrib.auth import authenticate,login,logout
#from django.contrib.auth.decorators import login_required
#template and URL
from django.template import loader
from django.http import HttpResponse,HttpResponseRedirect
from django.urls import reverse
from django.views.decorators.csrf import csrf_protect
import requests,json
# Error messages and Notifications for users
messages = { 
                  "empty": None,
                  "a": "Email Exist",
                  "b": "Username Exist",
                  "c": "Registration_Successful", 
                  "d": "Wrong Password",
                  "e": "Username does not Exist",
                  "f": "Sign in to Continue",
                  "g": "list successfully created",
                  "h": "You can't create new list on your present subscription Plan,Kindly upgrade to enjoy this benefit",
                  "i": "You can't add on your present subscription Plan,Kindly upgrade to enjoy this benefit",
                  "j": "list successfully removed",
                  "k": "You can't remove list on your present subscription Plan,Kindly upgrade to enjoy this benefit",
                  "l": "You can't rename list on your present subscription Plan,Kindly upgrade to enjoy this benefit",
                  "m": "Payment Successfully completed",
                  "n": "Payment not Successful, Click on your interested subscription to try again",
            }

def update(user_name,sub):  #Updating Users' information after payment
    present_user = get_object_or_404(User,username = user_name);
    profile = Profile.objects.get(owner = present_user); profile.sub = sub; profile.save()
    return HttpResponseRedirect(reverse("goals:index", args = (user_name,"m",)))
    
def confirm(request,user_name,txref):   #Validating Payment From Server-Side
    data = {
            "txref": txref,
            "SECKEY": "FLWSECK-55903e651ec0038cafed9a20673c8bfc-X", 
            "include_payment_entity": "1",
            }
    url = "https://ravesandboxapi.flutterwave.com/flwv3-pug/getpaidx/api/xrequery"
    r = requests.post(url, data = json.dumps(data),headers = {"Content-Type":"application/json"} )
    response = r.json()
    if response['status'] == 'success':
        if response['data']['chargecode'] == '00':
            #Updating Users' information after payment
            if (response['data']['amount'] == 5000):
                return update(user_name,"Premium")  
            elif (response['data']['amount'] == 1000):
                return update(user_name,"Starter")
    else:
        return HttpResponseRedirect(reverse("goals:index", args = (user_name,"n",)))
    
@csrf_protect
def home(request):  #Home view
    return HttpResponseRedirect(reverse("goals:register", args = ("empty",)))
 
@csrf_protect
def sign_up(request): #Sign Up
    user_name = request.POST.get("user name")
    mail = request.POST.get("email")
    pword =request.POST.get("password")
    if User.objects.filter(email = mail).exists(): #Checking if Email exist prior to sign up
        return HttpResponseRedirect(reverse("goals:register", args = ( "a",)))
    elif User.objects.filter(username = user_name).exists():   #Checking if username exist prior to sign up
        return HttpResponseRedirect(reverse("goals:register", args = ( "b",)))
    else:
        new_user = User(username = user_name, email = mail) ; 
        new_user.set_password(pword); new_user.save()
        new_profile = Profile(owner = new_user); new_profile.save()
        return HttpResponseRedirect(reverse("goals:register", args = ("c",)))

@csrf_protect
def sign_in(request):
    user_name = request.POST.get("user name")
    pword = request.POST.get("password")
    if User.objects.filter(username = user_name).exists(): #Checking if user is signed-up
        present_user = authenticate(username = user_name, password = pword)
        if present_user:      
            login(request,present_user)
            return HttpResponseRedirect(reverse("goals:index", args = (user_name,"empty",)))
        else:
            return HttpResponseRedirect(reverse("goals:register", args = ("d",)))
    else:
        return HttpResponseRedirect(reverse("goals:register", args = ("e",)))
        
def log_out(request):
    logout(request)
    return HttpResponseRedirect(reverse("goals:register",args = ("empty",)))

@csrf_protect   
def register(request,msg): #register view for sogn-up and sign in
    template = loader.get_template("goals/register.htm")
    context = {"message":messages[msg]}
    tmp = template.render(context,request)
    return HttpResponse(tmp)

import time
def index(request,user_name,msg): #Index view that displays list of all goals
    present_user = get_object_or_404(User,username = user_name); 
    mail = present_user.email; mail = str(mail)
    txref = "mlg" + str(time.time()); txref = txref.replace(".","7")
    if request.user.is_authenticated:
        profile = Profile.objects.get(owner = present_user); sub = profile.sub
        g_list = present_user.goals_list_set.all()
        template = loader.get_template("goals/index.htm")
        context = {"goals_categories":g_list,
                   "user":present_user, 
                   "message":messages[msg], 
                   "sub":sub,
                   "mail": mail,
                   "txref": txref,
                   }
        tmp = template.render(context,request)
        return HttpResponse(tmp)
    else:
        return HttpResponseRedirect(reverse("goals:register", args = ("f",)))

def list_detail(request,user_name,list_id): #list_detail displays goals under each list
    present_user = get_object_or_404(User,username = user_name)
    if request.user.is_authenticated:
        present_list = get_object_or_404(Goals_list,pk = list_id)
        present_goals = present_list.goals_set.all()
        template = loader.get_template("goals/list_details.htm")
        context = {"goals":present_goals,"category":present_list, "user":present_user}
        tmp = template.render(context,request)
        return HttpResponse(tmp)
    else:
        return HttpResponseRedirect(reverse("goals:register", args = ("f",)))

def add(request,user_name,list_id): #add view for creating new list and new goals
    present_user = get_object_or_404(User,username = user_name)
    if request.user.is_authenticated:
        list_id = int(list_id);
        profile = Profile.objects.get(owner = present_user); sub = profile.sub
        if not (list_id): # A list is about to be created
            if sub == "Premium" or (sub != "Premium" and profile.n_list < 1):
                new_list = Goals_list(owner = present_user,title = request.GET.get("new_list"))
                new_list.save(); profile.n_list += 1;profile.save()
                return HttpResponseRedirect(reverse("goals:index",args = (user_name,"g")))
            else:
                return HttpResponseRedirect(reverse("goals:index", args = (user_name,"h")))
        else: # A goal is to be created
            if (sub == "Basic" and profile.n_goals <= 7) or (profile.sub == "Starter" and profile.n_goals <= 12 ) or profile.sub == "Premium":
                goals_category = get_object_or_404(Goals_list,pk = list_id)
                tmp = goals_category.goals_set.create(text = request.GET.get("new_goal"),hit = 1)
                tmp.save(); profile.n_goals += 1; profile.save()
                return HttpResponseRedirect(reverse("goals:list_detail",args = (user_name,list_id,)))
            else:
                return HttpResponseRedirect(reverse("goals:index", args = (user_name,"i")))
    else:
        return HttpResponseRedirect(reverse("goals:register", args = ("f",)))
   
def remove(request,user_name,list_id,goal_id): #remove view for removing existing list and goals
    present_user = get_object_or_404(User,username = user_name)
    if request.user.is_authenticated:
        goal_id = int(goal_id); present_user = User.objects.get(username = user_name)
        profile = Profile.objects.get(owner = present_user); sub = profile.sub
        if not goal_id: # A list is about to be removed
            if sub == "Premium" or sub == "Basic":  #Basic been activated for removal is just for Debugging purposes
                killed = get_object_or_404(Goals_list,pk = list_id); killed.delete()
                profile.n_list -= 1; profile.save()
                return HttpResponseRedirect(reverse("goals:index", args = (user_name, "j")))
            else:
                return HttpResponseRedirect(reverse("goals:index", args = (user_name,"k")))
        else: # A goal is to be removed from a list, This has to be adjusted in a future project by including time: once in a month for basic, once in a week for starter
            killed = get_object_or_404(Goals,pk = goal_id) 
            killed.delete(); profile.n_goals -= 1; profile.save()
            return HttpResponseRedirect(reverse("goals:list_detail", args = (present_user.username,list_id,)))
    else:
        return HttpResponseRedirect(reverse("goals:register", args = ("f",)))

def rename(request,user_name,list_id,goal_id): #rename view for renaming existing goals and lists
    present_user = get_object_or_404(User,username = user_name)
    if request.user.is_authenticated:
        goal_id = int(goal_id); present_user = User.objects.get(username = user_name)
        template = loader.get_template("goals/rename.htm")
        profile = Profile.objects.get(owner = present_user); sub = profile.sub
        if not goal_id:     # A list is about to be removed
            if sub == "Premium":
                present_list = get_object_or_404(Goals_list,pk = list_id)
                context = {"list_text":present_list, "user":present_user}
            else:
                return HttpResponseRedirect(reverse("goals:index", args = (user_name,"l")))
        else: # A goal is to be renamed, This has to be adjusted in a future project, This has to be adjusted in a future project by including time: once in a month for basic, once in a week for starter
            present_goal = get_object_or_404(Goals,pk = goal_id)
            context = {"goals_text":present_goal, "user":present_user}
        tmp = template.render(context,request)
        return HttpResponse(tmp)
    else:
        return HttpResponseRedirect(reverse("goals:register", args = ("f",)))

def save(request,user_name,list_id,goal_id): #This view
    present_user = get_object_or_404(User,username = user_name)
    if request.user.is_authenticated:
        goal_id = int(goal_id); present_user = User.objects.get(username = user_name)
        if not goal_id: # A list is about to be renamed
            p_list = get_object_or_404(Goals_list,pk = list_id)
            p_list.title = request.GET.get("new_name"); p_list.save()
            return HttpResponseRedirect(reverse("goals:index", args = (present_user.username)))
        
        else: # A goal is to be renamed
            p_goal = get_object_or_404(Goals,pk = goal_id); 
            p_goal.text = request.GET.get("new_name"); p_goal.save()
            return HttpResponseRedirect(reverse("goals:list_detail", args = (present_user.username,list_id,)))
    else:
        return HttpResponseRedirect(reverse("goals:register", args = ("f",)))