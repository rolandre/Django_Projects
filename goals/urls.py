from django.conf.urls import url

from . import views

app_name = "goals"
urlpatterns = [
    url(r'^$',views.home,name = 'home'),
    url(r'^register/(?P<msg>[a-z]+)/$',views.register, name = 'register'),
    url(r'^confirm/(?P<user_name>\w+)/(?P<txref>\w+)/$',views.confirm, name = 'confirm'),
    url(r'^sign_up/$',views.sign_up, name = 'sign_up'),
    url(r'^sign_in/$',views.sign_in, name = 'sign_in'),
    url(r'^log_out/$',views.log_out, name = 'log_out'),
    url(r'^(?P<user_name>\w+)/(?P<msg>[a-z]+)/$', views.index, name = "index"),
    url(r'^(?P<user_name>\w+)/(?P<list_id>[0-9]+)/$',views.list_detail, name = 'list_detail'),
    url(r'^remove/(?P<user_name>\w+)/(?P<list_id>\w+)/(?P<goal_id>[0-9]+)/$',views.remove, name = "remove"),
    url(r'^rename/(?P<user_name>\w+)/(?P<list_id>\w+)/(?P<goal_id>\w+)/$',views.rename, name = "rename"),
    url(r'^add/(?P<user_name>\w+)/(?P<list_id>[0-9]+)/$',views.add, name = "add"),
    url(r'^save/(?P<user_name>\w+)/(?P<list_id>\w+)/(?P<goal_id>[0-9]+)/$',views.save, name = "save"),
        ]